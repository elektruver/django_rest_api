from django.urls import path

from api_v1 import SmokeView


urlpatterns = [
    path('smoke/', SmokeView.as_view()),
]
